const mongoose = require('mongoose');
const { Schema } = mongoose;


mongoose.connect("mongodb://localhost/tutorial",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

const db = mongoose.connection;
db.on('error', function () {
    console.log('connection error')
});

db.once('open', function () {
    console.log('successfully connection')
});

const kelasSchema = new Schema({
    judul: String,
    deskripsi: String,
    tglPosting: {
        type: Date,
        default: Date.now
    }
});


const Kelas = mongoose.model('Kelas', kelasSchema);

//carapertama
// const nodejs = new Kelas({
//     judul: 'nodejs',
//     deskripsi: "javascript runtime built on V8 jasaScript engine"
// })

// nodejs.save(function (error, data) {
//     if (Error) console.log(Error);

//     console.log(data);
//     console.log('successfully saved data on Database')
// })


// const express = new Kelas()
// express.judul = "express"
// express.deskripsi = "fast, unopinionated, minimalis framework"

// express.save(function (error, data) {
//     if (Error) console.log(Error);

//     console.log(data);
//     console.log('successfully saved data on Database')
// })

//caraketiga
// Kelas.create({
//     judul: "vuejs",
//     deskripsi: "the progressive javaScript framework"
// }, function (error, data) {
//     if (Error) console.log(Error);

//     console.log(data);
//     console.log('successfully created data on Database')
// })

// /find mengembalikan  nilai array, findOne mengembalikan object
// Kelas.findById("600d88e1bba65219a6219d45", function (error, data) {
//     if (Error) console.log(Error);

//     console.log(data);
// })


//metode execute
// const query = Kelas.find({ judul: 'nodejs' });
// query.exec(function (error, data) {
//     if (Error) console.log(Error);

//     console.log(data);
// })


const query = Kelas.find({ judul: 'nodejs' });
query.select('judul tglPosting')
query.exec(function (error, data) {
    if (Error) console.log(Error);

    console.log(data);
})
